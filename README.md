# Design at Wikimedia Foundation

Deployed at [design.wikimedia.org](https://design.wikimedia.org/).
Imported from of https://gerrit.wikimedia.org/r/plugins/gitiles/design/strategy/

## Conventions
New projects should be added to "Timeline" page and the appropriate category page under
"Initiatives".

## Development

## Blubber container image

Blubber images for design-strategy page.

### Local development

Build image locally:
```
DOCKER_BUILDKIT=1 docker build --target production -f .pipeline/blubber.yaml .
```

Run image:
```
docker run -p 8080:8080 <image name>
```

Visit `http://127.0.0.1:8080/`

### Publish new image version

To create a new image version merge your change into the master branch.

This triggers the publish-image pipeline. Image is available at `docker-registry.wikimedia.org/repos/sre/miscweb/design-strategy:<timestamp>`

## Deploy changes

- Merge changes to master branch.

- Update image version on [deployment-charts](https://gerrit.wikimedia.org/r/plugins/gitiles/operations/deployment-charts/%2B/refs/heads/master/helmfile.d/services/miscweb/values-design-strategy.yaml#3) with [latest value](https://gitlab.wikimedia.org/repos/sre/miscweb/design-strategy/-/jobs/). _The value is in the `#13` step in the job 'publish-image'._

- Add wmf-sre-collab (group) as reviewer and wait for merge.

- Deploy to production from the deployment server. 

See [WikiTech: Miscweb](https://wikitech.wikimedia.org/wiki/Miscweb#Deploy_to_Kubernets).
